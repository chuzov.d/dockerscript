#!/bin/bash

if [ "$(docker images -q $1)" != "" ]; then
	if [ "$(docker ps -q -f status=running -f name=$2)" == "" ]; then
		if [ "$(docker ps -a -q -f name=$2)" == "" ]; then
		docker run -d --name="$2" -v /etc/passwd:/etc/passwd:ro $1 /bin/bash -c "while true; do sleep 60; done"
		else
		docker start $2
		fi
	fi
	docker exec -it --user=$(id -u):$(id -g $(whoami)) $2 bash
else
echo "No images with this name found"
fi
